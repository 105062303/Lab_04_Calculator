<script>

    var temp=0;  //暫存被運算數

    var strOper=0;  //暫存運算符號

    var valueM=0;  //m+ m- mc mr 暫存使用

    var str;


    //數字鍵與小數點

    function numBtn(btn) 
    {
        var text = document.getElementById("screen”);
        text.value = btn;

        /*if(strOper==null) {
            document.getElementById("screen").value = str;
            strOper=0;
            return;
        };

       if (document.getElementById("screen").value==0) { 
            document.getElementById("screen").value = str;
        } 
        else {
            document.getElementById("screen").value += str;
        }
*/
    }

 function operBtn(str){

        //不按等號的連續運算

        equal();

        valueA=document.getElementById("screen").value;
        strOper=str;

        document.getElementById("screen").value=0;
    }

function equal(){   
    switch (strOper){
        case '+':
            document.getElementById("screen").value=parseFloat(valueA)+parseFloat(document.getElementById("screen").value);
            break;
        case '-':
            document.getElementById("screen").value=parseFloat(valueA)-parseFloat(document.getElementById("screen").value);
            break;
        case '*':
            document.getElementById("screen").value=parseFloat(valueA)*parseFloat(document.getElementById("screen").value);
            break;
        case '/':
            document.getElementById("screen").value=parseFloat(valueA)/parseFloat(document.getElementById("screen").value);
            break;
        default:
            break;
    }

    //防呆用

    temp=null;
    strOper=null;
}

function AllClear()             //Clear ALL entries!
 { Current = "0";
   value = "0";
   Operation = 0;                //clear operation
   Memory = "0";                  //clear memory
   document.Calculator.Display.value = Current;
 }

 function Operate(op)            //STORE OPERATION e.g. + * / etc.
 {
  if (op.indexOf("*") > -1) { Operation = 1 };       //codes for *
  if (op.indexOf("/") > -1) { Operation = 2 };       // slash (divide)
  if (op.indexOf("+") > -1) { Operation = 3 };       // sum
  if (op.indexOf("-") > -1) { Operation = 4 };       // difference

  Memory = Current;                 //store value
  Current = "";                     //or we could use "0"
  document.Calculator.Display.value = Current;
 }

 function Calculate()            //PERFORM CALCULATION (= button)
 { 
  if (Operation == 1) { Current = eval(Memory) * eval(Current) };
  if (Operation == 2) { Current = eval(Memory) / eval(Current) };
  if (Operation == 3) { Current = eval(Memory) + eval(Current) };
  if (Operation == 4) { Current = eval(Memory) - eval(Current) };
  Operation = 0;                //clear operation
  Memory    = "0";              //clear memory
  document.Calculator.Display.value = Current;
 }

 </script>